package com.muhundu.pprd.models;



import android.net.Uri;

public class Utilisateur {
    int id;
    String pseudonyme;
    String prenom;
    String nom;
    String postnom;
    String email;
    String telephone;
    String ville;
    String pays;
    String motdepasse;
    Uri imgProfil;

    public Utilisateur() {
    }


    public Utilisateur(int id, String pseudonyme, String prenom, String nom, String postnom, String email, String telephone, String ville, String pays, String motdepasse, Uri imgProfil) {
        this.id = id;
        this.pseudonyme = pseudonyme;
        this.prenom = prenom;
        this.nom = nom;
        this.postnom = postnom;
        this.email = email;
        this.telephone = telephone;
        this.ville = ville;
        this.pays = pays;
        this.motdepasse = motdepasse;
        this.imgProfil = imgProfil;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudonyme() {
        return pseudonyme;
    }

    public void setPseudonyme(String pseudonyme) {
        this.pseudonyme = pseudonyme;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPostnom() {
        return postnom;
    }

    public void setPostnom(String postnom) {
        this.postnom = postnom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    public Uri getImgProfil() {
        return imgProfil;
    }

    public void setImgProfil(Uri imgProfil) {
        this.imgProfil = imgProfil;
    }
}
