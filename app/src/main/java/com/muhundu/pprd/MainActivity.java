package com.muhundu.pprd;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;

import android.content.ActivityNotFoundException;
import android.content.SharedPreferences;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.muhundu.pprd.R;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.muhundu.pprd.adapter.NewsAdapter;
import com.muhundu.pprd.models.News;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolBar;
    ArrayList<News> informations;
    private RecyclerView recyclerView;
    private NewsAdapter newsAdapter;
    RecyclerView.LayoutManager manager;
    private FloatingActionButton fabAction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout=findViewById(R.id.drawer_layout);
        navigationView=findViewById(R.id.nav_view);
        toolBar=findViewById(R.id.toolbar5);
       recyclerView=findViewById(R.id.recycler_view);
       fabAction=findViewById(R.id.whatsapp_floting);



       setSupportActionBar(toolBar);

        Menu menu=navigationView.getMenu();

        navigationView.bringToFront();
        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setCheckedItem(R.id.nav_home);
        navigationView.setNavigationItemSelectedListener(this);


        SharedPreferences preferences=getSharedPreferences("loginUser",MODE_PRIVATE);
        boolean userLogin=preferences.getBoolean("userLogin",false);

        if(!userLogin) {
            startActivity(new Intent( MainActivity.this, LoginScreen.class));
            finish();
        }


        informations=new ArrayList<>();

        informations.add(new News(1,"PPRD: CELEBRATION DE L'ANNIVERSAIRE DE JOSEPH KABILA ET DE SA SOEUR JEANNETTE",
                "https://laprosperiteonline.net/wp-content/uploads/bfi_thumb/image007-14-3al0xvu5yyx5r4dqi93y16.jpg",
                "L'anniversaire de Joseph Kabila fait remonter à la surface ses plus proches collaborateurs que l'opinion n'a plus vus tous ces temps pour des raisons évidentes. Mais qui ont résisté à la transhumance et au débauchage monnayé. A notre Dame du Congo, on a pu revoir : E. Ramazani shadary l'organisateur de cette solennité,  Néhémie Mwilanya, Evariste Boshab, André Kimbuta, Bruno Tshibal,  Raymond Tshibanda, Jean Pierre Kambila, José Makila, les honorables Colette tshomba, Balamage, Kabange Numbi,les sénateurs, les anciens ministres et autres cadres du FCC ( impossible de citer tout le monde) étaient à la cathédrale Notre Dame du Congo, visage rayonnant de joie....",
                Long.parseLong("1618729200000"), 1, Long.parseLong("1618729200000")));
        informations.add(new News(2,"PPRD: E.RAMAZANI A REUNI LES PRÉSIDENTS DES GROUPES PARLEMENTAIRES DE SON PARTI.",
                "https://www.mediacongo.net/cache/bemba_mlc_19_0214_png_640_350_1.png",
                "Le secrétaire Permanent du Pprd a réuni mercredi les présidents des bureaux des groupes parlementaires du Pprd et alliés.\n" +
                        "Il était question pour le secrétaire Permanent du parti de Joseph Kabila Kabange d'échanger avec ses hôtes sur les réalités actuelles à l'assemblée nationale avant de donner des orientations sur les groupes parlementaires devant revenir au pprd.\n" +
                        "Par la même occasion, les participants ont fait le point sur les discussions au niveau de l'assemblée nationale ayant permis à l'opposition républicaine incarnée par le Fcc avec le pprd en tête d'obtenir  la présidence de 4 commissions permanentes de l'assemblée nationale. Notamment la commission de Suivi et évaluation, la commission des droits humains, la commission des infrastructures et le comité des Sages.\n" +
                        "De nouvelles orientations ont été données aux présidents des bureaux des groupes parlementaires pour les prochaines batailles parlementaires autour de  la direction des groupes parlementaires en tenant compte du poids Politique de chaque plateforme ou parti politique dans le respect du Règlement intérieur.\n" +
                        "Comme à toutes les réunions, les participants ont réitéré leur fidélité au président national du Pprd , Joseph Kabila Kabange.\n" +
                        "Cellule de communication.",
                Long.parseLong("1626332400000"), 1, Long.parseLong("1626332400000")));

        informations.add(new News(3,"PPRD: LE SECRÉTAIRE PERMANENT ANNONCE LA RESTAURATION DES ORGANES DU PARTI.",
                "https://www.mediacongo.net/cache/bemba_mlc_19_0214_png_640_350_1.png",
                "Emmanuel Ramazani shadary, le secrétaire Permanent du Pprd a présidé au siège du parti de Joseph Kabila et sur instruction de ce dernier,  le mardi 04 Mai 2021, une importante réunion à l'intention des secrétaires exécutifs provinciaux et communaux de la Tshangu et du Mont Amba ,respectivement conduits par Papy Epiana et Fatouma Djouma.\n" +
                        "Après la communication du secrétaire Permanent sur la situation politique, sociale et sécuritaire actuelle du pays, il a été question pour les participants de réfléchir sur le fonctionnement du parti et la restructuration imminente de différents organes du Pprd. Nouvelle chaleureusement acceuillie par les participants qui ont reçu des orientations pour un travail de fond à la base et avec la participation de celle-ci pour d'éventuelles propositions au sommet du parti.\n" +
                        "Dans les jours à venir, le Pprd entend réapparaître pour véritablement jouer son rôle de parti politique de l'opposition républicaine conformément à la pratique démocratique, a indiqué Shadary Ramazani invitant la base du parti à travers le pays à la fidélité au Président National, Joseph Kabila.\n" +
                        "Après les comités exécutifs provinciaux et communaux de la Tshangu et du Mont Amba, Emmanuel Ramazani shadary rencontrera dans les prochains jours ceux de la Funa et de la Lukunga dans le même cadre.\n" +
                        "Cellule de communication.",
                Long.parseLong("1611993600000"), 1, Long.parseLong("1611993600000")));


        recyclerView.setHasFixedSize(true);
        manager=new GridLayoutManager(MainActivity.this,1);
       recyclerView.setLayoutManager(manager);



        newsAdapter=new NewsAdapter(informations,this);
        recyclerView.setAdapter(newsAdapter);


        fabAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://api.whatsapp.com/send?phone=+243823223538";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_home:
                startActivity(new Intent(this,MainActivity.class));
                break;
            case R.id.nav_profile:
                startActivity(new Intent(this, ProfileScreen.class));
                break;
            case R.id.nav_cotisation:
                startActivity(new Intent(this, CotisationScreen.class));
                break;
            case R.id.nav_about:
                startActivity(new Intent(this, AboutScreen.class));
                break;
            case R.id.nav_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Salut, voici le lien de téléchargement de l'application PPRD";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "PPRD App");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Partager via"));
                break;
            case R.id.nav_logout:
               SharedPreferences.Editor editor=getSharedPreferences("loginUser",MODE_PRIVATE).edit();
                editor.putBoolean("userLogin",false);
                editor.apply();
                startActivity(new Intent(MainActivity.this,LoginScreen.class));
                finish();
                break;


        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.facebook_nav:
                try {
                    Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/455060804666469"));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://web.facebook.com/455060804666469"));
                    startActivity(intent);
                    e.printStackTrace();
                }
                break;
            case R.id.twitter_nav:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=pprdofficiel")));
                }catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/pprdofficiel")));
                }
                break;
        }
        return true;
    }
}