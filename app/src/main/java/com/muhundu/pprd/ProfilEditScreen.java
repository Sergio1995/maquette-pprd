package com.muhundu.pprd;

import com.muhundu.pprd.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;


import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilEditScreen extends AppCompatActivity {
    private static final int RESULT_LOAD_IMAGE_GALLERY=50;
    private static final int RESULT_LOAD_IMAGE_CAMERA=60;
    private static final int STORAGE_PERMISSION_REQUEST_CODE=30;
    AppCompatButton modificationButton;
    String picturePath;
    SharedPreferences preferences;
    String isFirstLoad=null;
    TextInputLayout username, nom, postnom,federation,ville,email,telephone, ancienMotDePasse, nouveauMotDePasse,prenom;
    AppCompatSpinner paysSpinner;
    CircleImageView imageProfile;
    TextView changeImage;
    private  String[] countries = new String[]{"Afrique du Sud", "Afghanistan", "Albanie", "Algérie", "Allemagne", "Andorre	Angola", "Antigua-et-Barbuda", "Arabie Saoudite", "Argentine", "Arménie", "Australie", "Autriche", "Azerbaïdjan", "Bahamas", "Bahreïn", "Bangladesh", "Barbade", "Belgique", "Belize", "Bénin", "Bhoutan", "Biélorussie", "Birmanie", "Bolivie", "Bosnie-Herzégovine", "Botswana", "Brésil", "Brunei", "Bulgarie", "Burkina Faso", "Burundi", "Gitega", "Cambodge", "Cameroun", "Canada", "Cap-Vert", "Chili", "Chine", "Chypre", "Colombie", "Comores", "Corée du Nord", "Corée du Sud", "Costa Rica	Côte d’Ivoire", "Croatie", "Cuba", "Danemark", "Djibouti", "Dominique", "Égypte", "Émirats arabes unis", "Équateur", "Érythrée", "Espagne", "Eswatini", "Estonie", "États-Unis", "Éthiopie", "Fidji", "Finlande", "France", "Gabon", "Gambie", "Géorgie", "Ghana", "Grèce", "Grenade", "Guatemala", "Guinée", "Guinée équatoriale", "Guinée-Bissau", "Guyana", "Haïti", "Honduras", "Hongrie", "Îles Cook", "Îles Marshall", "Inde", "Indonésie", "Irak", "Iran", "Irlande", "Islande", "Israël", "Italie", "Jamaïque", "Japon", "Jordanie", "Kazakhstan", "Kenya", "Kirghizistan	Kiribati", "Koweït", "Laos", "Lesotho", "Lettonie", "Liban", "Liberia", "Libye", "Liechtenstein", "Lituanie", "Vilnius", "Luxembourg", "Macédoine", "Madagascar", "Malaisie", "Malawi", "Maldives", "Mali", "Malte", "Maroc", "Maurice", "Mauritanie", "Mexique", "Micronésie", "Moldavie", "Monaco", "Mongolie", "Monténégro", "Mozambique", "Namibie", "Nauru", "Népal", "Nicaragua", "Niger", "Nigeria", "Niue", "Norvège", "Nouvelle-Zélande", "Oman", "Ouganda", "Ouzbékistan", "Pakistan", "Palaos", "Palestine", "Panama", "Papouasie-Nouvelle-Guinée", "Paraguay", "Pays-Bas", "Pérou", "Philippines", "Pologne", "Portugal", "Qatar", "République centrafricaine", "République démocratique du Congo", "République Dominicaine", "République du Congo", "République tchèque", "Roumanie", "Royaume-Uni", "Russie", "Rwanda", "Saint-Kitts-et-Nevis", "Saint-Vincent-et-les-Grenadines", "Sainte-Lucie", "Saint-Marin", "Salomon", "Salvador", "Samoa", "São Tomé-et-Principe", "Sénégal", "Serbie", "Seychelles", "Sierra Leone", "Singapour", "Slovaquie", "Slovénie", "Somalie", "Soudan", "Soudan du Sud", "Sri Lanka	", "Suède", "Suisse", "Suriname", "Syrie", "Tadjikistan", "Tanzanie", "Tchad", "Thaïlande", "Timor oriental", "Togo", "Tonga", "Trinité-et-Tobago", "Tunisie", "Turkménistan", "Turquie", "Tuvalu", "Ukraine", "Uruguay", "Vanuatu", "Vatican", "Venezuela", "Viêt Nam", "Yémen", "Zambie", "Zimbabwe"};

    BitmapFactory.Options options = new BitmapFactory.Options();
    Uri exitedImageUri;

    private RequestPermissionHandler mRequestPermissionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profil_edit_screen);
        modificationButton=findViewById(R.id.valide_modification_button);
        username=findViewById(R.id.username);
        nom=findViewById(R.id.nom);
       postnom=findViewById(R.id.postnom);
        ville=findViewById(R.id.ville);
       ancienMotDePasse=findViewById(R.id.old_password);
        nouveauMotDePasse=findViewById(R.id.new_password);
        telephone=findViewById(R.id.phone_number);
        federation=findViewById(R.id.federation);
        paysSpinner=findViewById(R.id.spinner_pays);
        email=findViewById(R.id.email);
        prenom=findViewById(R.id.prenom_user);
        imageProfile=findViewById(R.id.profile_image);
        changeImage=findViewById(R.id.image_profile_change);

        mRequestPermissionHandler = new RequestPermissionHandler();

        Toolbar toolbar=findViewById(R.id.toolbar5);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        options.inSampleSize = 6;
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        ArrayAdapter<CharSequence> adapter =new ArrayAdapter<>(this,R.layout.dropdown_list_item,countries);
        paysSpinner.setAdapter(adapter);

        preferences=getSharedPreferences("SignupUser",MODE_PRIVATE);

        username.getEditText().setText(preferences.getString("username",null));
        nom.getEditText().setText(preferences.getString("nom",null));
        postnom.getEditText().setText(preferences.getString("postnom",null));
        ville.getEditText().setText(preferences.getString("ville",null));
        telephone.getEditText().setText(preferences.getString("telephone",null));
        federation.getEditText().setText(preferences.getString("federation",null));
        email.getEditText().setText(preferences.getString("email",null));
        prenom.getEditText().setText(preferences.getString("prenom",null));

        if(preferences.getString("imageProfile","")==null)
            imageProfile.setImageResource(R.drawable.avatar_user_circle_icon);
        else
            imageProfile.setImageBitmap(BitmapFactory.decodeFile(preferences.getString("imageProfile",""),options));


        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertChooseImage();
            }
        });



        modificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences=getSharedPreferences("SignupUser",MODE_PRIVATE);

                SharedPreferences.Editor editor=preferences.edit();

                if(!ancienMotDePasse.getEditText().getText().toString().isEmpty()) {
                    if (ancienMotDePasse.getEditText().getText().toString().equals(preferences.getString("password", null))) {
                        editor.putString("username", username.getEditText().getText().toString());
                        editor.putString("email", email.getEditText().getText().toString());
                        editor.putString("nom", nom.getEditText().getText().toString());
                        editor.putString("postnom", postnom.getEditText().getText().toString());
                        editor.putString("telephone", telephone.getEditText().getText().toString());
                        editor.putString("ville", ville.getEditText().getText().toString());
                        editor.putString("pays", paysSpinner.getSelectedItem().toString());
                        editor.putString("password", nouveauMotDePasse.getEditText().getText().toString());
                        editor.putString("federation", federation.getEditText().getText().toString());
                        editor.putString("prenom", prenom.getEditText().getText().toString());
                        editor.apply();
                    } else {
                        ancienMotDePasse.setError("Votre ancien mot de passe est incorrect");
                        ancienMotDePasse.requestFocus();
                        return;
                    }
                }else {
                        editor.putString("username",username.getEditText().getText().toString());
                        editor.putString("email",email.getEditText().getText().toString());
                        editor.putString("nom",nom.getEditText().getText().toString());
                        editor.putString("postnom",postnom.getEditText().getText().toString());
                        editor.putString("telephone",telephone.getEditText().getText().toString());
                        editor.putString("ville",ville.getEditText().getText().toString());
                        editor.putString("pays",paysSpinner.getSelectedItem().toString());
                        editor.putString("federation",federation.getEditText().getText().toString());
                        editor.putString("prenom", prenom.getEditText().getText().toString());
                        editor.apply();
                    }
                    Intent intent=new Intent(ProfilEditScreen.this,ProfileScreen.class);
                intent.putExtra("isSet",true);
                startActivity(intent);
                finish();
            }
        });

        changeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertChooseImage();

            }
        });



        exitedImageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + getApplicationContext().getResources().getResourcePackageName(R.drawable.avatar_user_circle_icon)
                + '/' + getApplicationContext().getResources().getResourceTypeName(R.drawable.avatar_user_circle_icon)
                + '/' + getApplicationContext().getResources().getResourceEntryName(R.drawable.avatar_user_circle_icon) );

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        mRequestPermissionHandler.onRequestPermissionsResult(requestCode, permissions,
                grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // check whether storage permission granted or not.
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // do what you want;
                        takeFromCamera();
                    }
                }
                break;
            case 20:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    takeFromCamera();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RESULT_LOAD_IMAGE_GALLERY){
            if(resultCode==RESULT_OK && null != data){

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();


                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imageProfile.setImageBitmap(bitmap);



                    SharedPreferences.Editor editor =preferences.edit();
                    editor.putString("imageProfile", picturePath);
                    editor.apply();
            }
        }



        if(requestCode==RESULT_LOAD_IMAGE_CAMERA){
            if(resultCode==RESULT_OK && data!=null){

                Bundle bundle=data.getExtras();
                Bitmap captureImage = (Bitmap) bundle.get("data");
                imageProfile.setImageBitmap(captureImage);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                captureImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] databaos = baos.toByteArray();

                String[] projection = { MediaStore.Images.Media.DATA };
                Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        projection, null, null, null);
                int column_index_data = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToLast();

                picturePath=cursor.getString(column_index_data);



                SharedPreferences.Editor editor =preferences.edit();
                editor.putString("imageProfile", picturePath);
                editor.apply();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void handleButtonClicked(){
        mRequestPermissionHandler.requestPermission(this, new String[] {
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
        }, 123, new RequestPermissionHandler.RequestPermissionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(ProfilEditScreen.this, "request permission success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailed() {
                Toast.makeText(ProfilEditScreen.this, "request permission failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void alertChooseImage(){

        handleButtonClicked();

        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        LayoutInflater inflater=getLayoutInflater();
        View dialogView=inflater.inflate(R.layout.choose_image,null);
        builder.setCancelable(true);
        builder.setView(dialogView);

        CardView mobileGallery=dialogView.findViewById(R.id.cardview_gallery);
        CardView cardCamera=dialogView.findViewById(R.id.cardview_camera);

        AlertDialog chooseImageDialog=builder.create();
        chooseImageDialog.show();


        mobileGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Choisir l'image"), RESULT_LOAD_IMAGE_GALLERY);
                chooseImageDialog.cancel();
            }
        });

        cardCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkAndRequestPermission()) {
                    takeFromCamera();
                    chooseImageDialog.cancel();
                }
            }
        });


    }

    private  boolean checkAndRequestPermission(){
        if(ContextCompat.checkSelfPermission(ProfilEditScreen.this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
            if(Build.VERSION.SDK_INT >=23){
                int cameraPermission= ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA);
                if(cameraPermission==PackageManager.PERMISSION_DENIED){
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},20);
                    return false;
                }
            }

        }
        return true;
    }
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("imageProfile", picturePath);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        imageProfile.setImageBitmap(BitmapFactory.decodeFile(savedInstanceState.getString("imageProfile")));
    }

    private void takeFromCamera(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager())!=null) {
            startActivityForResult(intent, RESULT_LOAD_IMAGE_CAMERA);
        }
    }
}