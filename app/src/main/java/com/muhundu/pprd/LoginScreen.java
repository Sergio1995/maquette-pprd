package com.muhundu.pprd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.muhundu.pprd.R;

import com.google.android.material.textfield.TextInputLayout;


public class LoginScreen extends AppCompatActivity {
    TextInputLayout username,password;
    TextView forgotPassword, goToSignup;
    AppCompatButton loginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        username=findViewById(R.id.email);
        password=findViewById(R.id.password);
        loginButton=findViewById(R.id.login_button);
        forgotPassword=findViewById(R.id.forgot_password);
        goToSignup=findViewById(R.id.login_to_signup);

        SharedPreferences preferences=getSharedPreferences("loginUser",MODE_PRIVATE);
        boolean userLogin=preferences.getBoolean("userLogin",false);

        if(userLogin) {
            startActivity(new Intent(LoginScreen.this, MainActivity.class));
            finish();
        }

        goToSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginScreen.this,SignupScreen.class));
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUser();
            }
        });

    }

    private void LogUser(){
        if(username.getEditText().getText().toString().trim().isEmpty()){
            username.setError("Champ obligatoire");
            username.requestFocus();
            return;
        }
        if(password.getEditText().getText().toString().trim().isEmpty()){
            username.setError("Champ obligatoire");
            username.requestFocus();
            return;
        }
        SharedPreferences preferences=getSharedPreferences("SignupUser",MODE_PRIVATE);
        String passwordExist=preferences.getString("password",null);
        String usernameExist=preferences.getString("username",null);
        String emailExist=preferences.getString("email",null);

        if(passwordExist!=null && usernameExist!=null || emailExist!=null){
            if(usernameExist.equals(username.getEditText().getText().toString()) || emailExist.equals(username.getEditText().getText().toString()) && passwordExist.equals(password.getEditText().getText().toString())){
                startActivity(new Intent(LoginScreen.this, MainActivity.class));
                SharedPreferences.Editor editor=getSharedPreferences("loginUser",MODE_PRIVATE).edit();
                editor.putBoolean("userLogin",true);
                editor.apply();
                finish();
            }else{
                alertLoginError();
            }

        }else{
            alertLoginError();
        }
    }

    private void alertLoginError(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        LayoutInflater inflater=getLayoutInflater();
        View dialogView=inflater.inflate(R.layout.login_error,null);
        builder.setCancelable(true);
        builder.setView(dialogView);

        ImageView cancelImage=dialogView.findViewById(R.id.cancel_image);


        AlertDialog chooseImageDialog=builder.create();
        chooseImageDialog.show();



        cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImageDialog.cancel();
            }
        });


    }
}