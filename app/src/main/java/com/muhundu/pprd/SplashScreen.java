package com.muhundu.pprd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import com.muhundu.pprd.R;


public class SplashScreen extends AppCompatActivity {
  private static int SPLASH_SCREEN=5000;
  TextView poweredMessage;
  ImageView logo;
  Animation topAnim, bottomAnim;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash_screen);




    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        startActivity(new Intent(SplashScreen.this,LoginScreen.class));
        finish();
      }
    },SPLASH_SCREEN);
  }
}