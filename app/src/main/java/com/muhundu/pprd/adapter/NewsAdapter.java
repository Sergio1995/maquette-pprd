package com.muhundu.pprd.adapter;

import com.muhundu.pprd.R;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muhundu.pprd.models.News;

import com.muhundu.pprd.ReadArticle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    ArrayList<News> newsArraylist;
    Context context;
    LayoutInflater inflater;

    public NewsAdapter(ArrayList<News> newsArraylist, Context context) {
        this.newsArraylist = newsArraylist;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=inflater.from(context).inflate(R.layout.news_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        News news=newsArraylist.get(position);
        holder.newsTitle.setText(news.getTitle());
        holder.newsContent.setText(news.getContent());
       // Glide.with(context).load(news.getImageUrl()).into(holder.newsImg);

        if(news.getId()==1)
            holder.newsImg.setImageResource(R.mipmap.article1);
        else if(news.getId()==2)
            holder.newsImg.setImageResource(R.mipmap.article2);
        else if(news.getId()==3)
            holder.newsImg.setImageResource(R.mipmap.article3);

        SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy", Locale.CANADA_FRENCH);
        Date now=new Date(news.getDatePublished());
        String dateFormatted=formatter.format(now);
        holder.newsDatePulished.setText("Publié le "+dateFormatted);

      /*  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Locale LocaleBylanguageTag = null;
            LocaleBylanguageTag = Locale.forLanguageTag("fr");
            TimeAgoMessages messages = new TimeAgoMessages.Builder().withLocale(LocaleBylanguageTag).build();

            String publishedDateString= TimeAgo.using(news.getDatePublished(),messages);
            holder.newsDatePulished.setText(publishedDateString);
        }
      */
      holder.newsContainer.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              Intent intent=new Intent(context, ReadArticle.class);
              intent.putExtra("id",news.getId());
              intent.putExtra("title",news.getTitle());
              intent.putExtra("content",news.getContent());
              intent.putExtra("imageUrl",news.getImageUrl());
              intent.putExtra("datePublished",news.getDatePublished());

              context.startActivity(intent);
          }
      });

    }

    @Override
    public int getItemCount() {
        return newsArraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
            private ImageView newsImg;
            private TextView newsTitle,newsContent, newsDatePulished;
            RelativeLayout newsContainer;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            newsImg=itemView.findViewById(R.id.image_news);
            newsTitle=itemView.findViewById(R.id.news_title);
            newsContent=itemView.findViewById(R.id.news_content);
            newsDatePulished=itemView.findViewById(R.id.news_date_published);
            newsContainer=itemView.findViewById(R.id.news_container);

        }
    }
}
