package com.muhundu.pprd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import com.muhundu.pprd.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;


public class SignupScreen extends AppCompatActivity {

    TextInputLayout username, email, telephone, ville, pays, password, confirm;
    AppCompatSpinner paysSpinner;
    TextView goToLogin;
    String addressMail=null;
    AppCompatButton signupButton;
    private  String[] countries = new String[]{"Afrique du Sud", "Afghanistan", "Albanie", "Algérie", "Allemagne", "Andorre	Angola", "Antigua-et-Barbuda", "Arabie Saoudite", "Argentine", "Arménie", "Australie", "Autriche", "Azerbaïdjan", "Bahamas", "Bahreïn", "Bangladesh", "Barbade", "Belgique", "Belize", "Bénin", "Bhoutan", "Biélorussie", "Birmanie", "Bolivie", "Bosnie-Herzégovine", "Botswana", "Brésil", "Brunei", "Bulgarie", "Burkina Faso", "Burundi", "Gitega", "Cambodge", "Cameroun", "Canada", "Cap-Vert", "Chili", "Chine", "Chypre", "Colombie", "Comores", "Corée du Nord", "Corée du Sud", "Costa Rica	Côte d’Ivoire", "Croatie", "Cuba", "Danemark", "Djibouti", "Dominique", "Égypte", "Émirats arabes unis", "Équateur", "Érythrée", "Espagne", "Eswatini", "Estonie", "États-Unis", "Éthiopie", "Fidji", "Finlande", "France", "Gabon", "Gambie", "Géorgie", "Ghana", "Grèce", "Grenade", "Guatemala", "Guinée", "Guinée équatoriale", "Guinée-Bissau", "Guyana", "Haïti", "Honduras", "Hongrie", "Îles Cook", "Îles Marshall", "Inde", "Indonésie", "Irak", "Iran", "Irlande", "Islande", "Israël", "Italie", "Jamaïque", "Japon", "Jordanie", "Kazakhstan", "Kenya", "Kirghizistan	Kiribati", "Koweït", "Laos", "Lesotho", "Lettonie", "Liban", "Liberia", "Libye", "Liechtenstein", "Lituanie", "Vilnius", "Luxembourg", "Macédoine", "Madagascar", "Malaisie", "Malawi", "Maldives", "Mali", "Malte", "Maroc", "Maurice", "Mauritanie", "Mexique", "Micronésie", "Moldavie", "Monaco", "Mongolie", "Monténégro", "Mozambique", "Namibie", "Nauru", "Népal", "Nicaragua", "Niger", "Nigeria", "Niue", "Norvège", "Nouvelle-Zélande", "Oman", "Ouganda", "Ouzbékistan", "Pakistan", "Palaos", "Palestine", "Panama", "Papouasie-Nouvelle-Guinée", "Paraguay", "Pays-Bas", "Pérou", "Philippines", "Pologne", "Portugal", "Qatar", "République centrafricaine", "République démocratique du Congo", "République Dominicaine", "République du Congo", "République tchèque", "Roumanie", "Royaume-Uni", "Russie", "Rwanda", "Saint-Kitts-et-Nevis", "Saint-Vincent-et-les-Grenadines", "Sainte-Lucie", "Saint-Marin", "Salomon", "Salvador", "Samoa", "São Tomé-et-Principe", "Sénégal", "Serbie", "Seychelles", "Sierra Leone", "Singapour", "Slovaquie", "Slovénie", "Somalie", "Soudan", "Soudan du Sud", "Sri Lanka	", "Suède", "Suisse", "Suriname", "Syrie", "Tadjikistan", "Tanzanie", "Tchad", "Thaïlande", "Timor oriental", "Togo", "Tonga", "Trinité-et-Tobago", "Tunisie", "Turkménistan", "Turquie", "Tuvalu", "Ukraine", "Uruguay", "Vanuatu", "Vatican", "Venezuela", "Viêt Nam", "Yémen", "Zambie", "Zimbabwe"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_screen);
        username=findViewById(R.id.username);
        email=findViewById(R.id.email);
        telephone=findViewById(R.id.phone_number);
        ville=findViewById(R.id.ville);
        password=findViewById(R.id.password);
        confirm=findViewById(R.id.confirm_password);
        signupButton=findViewById(R.id.signup_button);
        goToLogin=findViewById(R.id.signup_to_login);
        paysSpinner=findViewById(R.id.spinner_pays);


        ArrayAdapter<CharSequence> adapter =new ArrayAdapter<>(this,R.layout.dropdown_list_item,countries);
        paysSpinner.setAdapter(adapter);


        SharedPreferences preferences=getSharedPreferences("loginUser",MODE_PRIVATE);
        boolean userLogin=preferences.getBoolean("userLogin",false);

        if(userLogin) {
            startActivity(new Intent(SignupScreen.this, MainActivity.class));
            finish();
        }

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

        goToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupScreen.this,LoginScreen.class));
            }
        });
    }

    private void registerUser(){
        if(username.getEditText().getText().toString().trim().isEmpty()){
            username.setError("Champ obligatoire");
            username.requestFocus();
            return;
        }
        if(telephone.getEditText().getText().toString().trim().isEmpty()){
            telephone.setError("Champ obligatoire");
            telephone.requestFocus();
            return;
        }
        if(ville.getEditText().getText().toString().trim().isEmpty()){
            ville.setError("Champ obligatoire");
            ville.requestFocus();
            return;
        }
        if(paysSpinner.getSelectedItem().toString().trim().isEmpty()){
            paysSpinner.requestFocus();
            return;
        }

        if(password.getEditText().getText().toString().trim().isEmpty()){
            password.setError("Champ obligatoire");
            password.requestFocus();
            return;
        }
        if(password.getEditText().getText().toString().trim().length()<6){
            password.setError("Mot de passe doit avoir minimum 6 caratères");
            password.requestFocus();
            return;
        }
        if(!confirm.getEditText().getText().toString().trim().equals(password.getEditText().getText().toString())){
            confirm.setError("Le mot de passe ne correspond pas");
            confirm.requestFocus();
            return;
        }

        if(email.getEditText().getText().toString().trim().isEmpty()){
            addressMail=null;
        }else{
            addressMail=email.getEditText().getText().toString();
        }



        SharedPreferences preferences=getSharedPreferences("SignupUser",MODE_PRIVATE);


        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("username",username.getEditText().getText().toString());
        editor.putString("email",addressMail);
        editor.putString("telephone",telephone.getEditText().getText().toString());
        editor.putString("ville",ville.getEditText().getText().toString());
        editor.putString("pays",paysSpinner.getSelectedItem().toString());
        editor.putString("password",password.getEditText().getText().toString());
        editor.putString("nom",null);
        editor.putString("postnom",null);
        editor.putString("federation",null);
        editor.putString("prenom", null);
        editor.putString("profilImage", null);
        editor.apply();

        startActivity(new Intent(SignupScreen.this,LoginScreen.class));
        finish();

    }
}