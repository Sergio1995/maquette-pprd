package com.muhundu.pprd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import com.muhundu.pprd.R;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class CotisationScreen extends AppCompatActivity {
    private TextInputLayout montantDonation;
    private TextInputEditText montantDonationEdittxt;
    private AutoCompleteTextView act_intitule;
    private AppCompatButton payementButton;
    private TextView montantCotosationText;
    private AppCompatRadioButton cotisationRadioButton,donationRadioButton;
    private RadioGroup radioGroupDevise;
    String intitule=null;
    String hintMontantDonationUsd="Montant donation en USD";
    String hintMontantDonationCdf="Montant donation en FC";
    String hintMontantCotisationUsd="Cotisation 1$/mois";
    String hintMontantCotisationCdf="Cotisation 2100FC/mois";
    String devise="usd";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotisation_screen);

        Toolbar toolbar=findViewById(R.id.toolbar5);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        payementButton=findViewById(R.id.payer);
        montantCotosationText=findViewById(R.id.montant_cotisation_txt);
        montantDonation=findViewById(R.id.montant);
        montantDonationEdittxt=findViewById(R.id.montant_donation_edittext);
        radioGroupDevise=findViewById(R.id.devise_radiogroup);
        donationRadioButton=findViewById(R.id.radio_button_donation);
        cotisationRadioButton=findViewById(R.id.radio_button_cotisation);


        radioGroupDevise.check(R.id.radio_button_usd);
        montantDonation.setHint(hintMontantDonationUsd);
        montantCotosationText.setVisibility(View.VISIBLE);
        montantDonation.setVisibility(View.INVISIBLE);
        montantCotosationText.setText(hintMontantCotisationUsd);
        payementButton.setText("Cotiser");
        cotisationRadioButton.setChecked(true);
        donationRadioButton.setChecked(false);
        cotisationRadioButton.setBackground(getResources().getDrawable(R.drawable.selected_radio_button));
        cotisationRadioButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));






        radioGroupDevise.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.radio_button_fc:
                        montantCotosationText.setText(hintMontantCotisationCdf);
                        montantDonation.setHint(hintMontantDonationCdf);
                        devise="cdf";
                        break;
                    case R.id.radio_button_usd:
                        montantCotosationText.setText(hintMontantCotisationUsd);
                        montantDonation.setHint(hintMontantDonationUsd);
                        devise="usd";
                        break;
                }
            }
        });
        cotisationRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                montantDonation.setVisibility(View.GONE);
                montantCotosationText.setVisibility(View.VISIBLE);
                intitule="cotisation";
                cotisationRadioButton.setBackground(getResources().getDrawable(R.drawable.selected_radio_button));
                cotisationRadioButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                donationRadioButton.setTextColor(getResources().getColor(R.color.colorWhite));
                donationRadioButton.setBackground(getResources().getDrawable(R.drawable.transparent_background));
                payementButton.setText("Cotiser");
                cotisationRadioButton.setChecked(true);
                donationRadioButton.setChecked(false);
            }
        });

        donationRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                montantDonation.setVisibility(View.VISIBLE);
                montantCotosationText.setVisibility(View.GONE);
                donationRadioButton.setBackground(getResources().getDrawable(R.drawable.selected_radio_button));
                donationRadioButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                cotisationRadioButton.setTextColor(getResources().getColor(R.color.colorWhite));
                cotisationRadioButton.setBackground(getResources().getDrawable(R.drawable.transparent_background));
                intitule="donation";
                payementButton.setText("Donner");
                cotisationRadioButton.setChecked(false);
                donationRadioButton.setChecked(true);
            }
        });

montantDonationEdittxt.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(donationRadioButton.isChecked()) {
            if (radioGroupDevise.getCheckedRadioButtonId() == R.id.radio_button_fc) {
                if (montantCotosationText.getText().toString().length() < 3) {
                    montantDonation.setError("Montant donation invalide");
                } else {
                    montantDonation.setErrorEnabled(false);

                }
            } else if(radioGroupDevise.getCheckedRadioButtonId() == R.id.radio_button_usd) {
                if (montantCotosationText.getText().toString().isEmpty()) {
                    montantDonation.setError("Montant donation invalide");
                } else {
                    montantDonation.setErrorEnabled(false);

                }
            }
        }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
});

        payementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (donationRadioButton.isChecked()) {
                    if (radioGroupDevise.getCheckedRadioButtonId() == R.id.radio_button_fc) {
                        if (montantCotosationText.getText().toString().length() < 3) {
                            montantDonation.setError("Montant donation invalide");

                        } else {
                            montantDonation.setErrorEnabled(false);
                            alertChoosePayement();
                        }
                    } else if (radioGroupDevise.getCheckedRadioButtonId() == R.id.radio_button_usd) {
                        if (montantCotosationText.getText().toString().isEmpty()) {
                            montantDonation.setError("Montant donation invalide");
                        } else {
                            montantDonation.setErrorEnabled(false);
                            alertChoosePayement();
                        }
                    } else {
                        alertChoosePayement();
                    }


                }else{
                    alertChoosePayement();
                }
            }
        });
    }

    private void alertChoosePayement(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        LayoutInflater inflater=getLayoutInflater();
        View dialogView=inflater.inflate(R.layout.payement_choice,null);
        builder.setCancelable(true);
        builder.setView(dialogView);

        CardView mobilePayement=dialogView.findViewById(R.id.cardview_mobile_money);
        CardView cardPayement=dialogView.findViewById(R.id.cardview_credit_card);

        AlertDialog chooseImageDialog=builder.create();
        chooseImageDialog.show();


        mobilePayement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CotisationScreen.this,MobilePayement.class));
                chooseImageDialog.cancel();
            }
        });

        cardPayement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CotisationScreen.this,CardPayement.class));
                chooseImageDialog.cancel();
            }
        });


    }
}