package com.muhundu.pprd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;



public class ProfileScreen extends AppCompatActivity {
  EditText username,password;
  TextView fullName, ville, pays, telephone, email, prenom, nom, postnom, federation, usernametext;
  AppCompatButton updateButton;
  ImageView profileImage;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile_screen);
    fullName=findViewById(R.id.fullname);
    ville=findViewById(R.id.address_user);
    pays=findViewById(R.id.country_user);
    telephone=findViewById(R.id.telephone);
    prenom=findViewById(R.id.prenom_user);
    nom=findViewById(R.id.nom_user);
    postnom=findViewById(R.id.postnom_user);
    federation=findViewById(R.id.federation);
    usernametext=findViewById(R.id.username_user);
    email=findViewById(R.id.email_user);
    profileImage=findViewById(R.id.profile_image);
    updateButton=findViewById(R.id.modifier_profil);
    Toolbar toolbar=findViewById(R.id.toolbar5);
    
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    SharedPreferences preferences=getSharedPreferences("SignupUser",MODE_PRIVATE);
    fullName.setText(preferences.getString("username","")+" "+preferences.getString("nom",""));
    ville.setText(preferences.getString("ville",""));
    pays.setText(preferences.getString("pays",""));
    telephone.setText(preferences.getString("telephone",""));
    usernametext.setText(preferences.getString("username",""));
    federation.setText(preferences.getString("federation",""));
    nom.setText(preferences.getString("nom",""));
    postnom.setText(preferences.getString("postnom",""));
    prenom.setText(preferences.getString("prenom",""));
    email.setText(preferences.getString("email",""));

    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inSampleSize = 6;
    options.inScaled = false;
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

    if(preferences.getString("imageProfile",null)==null)
      profileImage.setImageResource(R.drawable.avatar_user_circle_icon);
    else
      profileImage.setImageBitmap(BitmapFactory.decodeFile(preferences.getString("imageProfile",null),options));

    updateButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(ProfileScreen.this,ProfilEditScreen.class));
      }
    });
  }
}