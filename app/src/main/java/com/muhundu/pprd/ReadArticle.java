package com.muhundu.pprd;

import com.muhundu.pprd.R;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ReadArticle extends AppCompatActivity {
    ImageView img;
    TextView title, content, datePublished;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_article);

        img=findViewById(R.id.details_img);
        title=findViewById(R.id.details_title);
        content=findViewById(R.id.details_content);
        datePublished=findViewById(R.id.date_published);

        Toolbar toolbar=findViewById(R.id.toolbar5);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent=getIntent();

        title.setText(intent.getStringExtra("title"));
        content.setText(intent.getStringExtra("content"));


        SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy", Locale.CANADA_FRENCH);
        Date now=new Date(intent.getLongExtra("datePublished",0));
        String dateFormatted=formatter.format(now);
       datePublished.setText("Publié le "+dateFormatted);

       switch (intent.getIntExtra("id",0)){
           case 1:
           img.setImageResource(R.mipmap.article1);
           break;
        case 2:
               img.setImageResource(R.mipmap.article2);
               break;
           case 3:
               img.setImageResource(R.mipmap.article3);
               break;
       }

    }
}